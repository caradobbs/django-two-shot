from django.contrib import admin
from receipts.models import ExpenseCategory
from receipts.models import Receipt
from receipts.models import Account

# Register your models here.
admin.site.register(ExpenseCategory)
admin.site.register(Receipt)
admin.site.register(Account)
